/*
 *  tas.c
 *  packing-satmod
 *
 *  Created by St�phane on 11/11/14.
 *  Copyright 2014 __MyCompanyName__. All rights reserved.
 *
 */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
//#include <tcl.h>

#include "heap.h"
#include "utils.h"


Heap allocHeap (int size, int (*lt)(int, int)) {
    Heap F;

    F = malloc(sizeof(struct heap));
    assert(F != NULL);

    F->size = size;
    F->n = 0;
    F->val = malloc(size*sizeof(int));
    assert(F->val != NULL);
    F->ind = malloc(size*sizeof(int));
    assert(F->ind != NULL);

    F->lt = lt;

    return F;
}


void heapSetCompare(Heap F, int (*lt)(int, int)) {
    F->lt = lt;
}


void freeHeap(Heap F) {
    free(F->val);
    free(F->ind);
    free(F);
}


void resetHeap(Heap F) {
    F->n = 0;
}


void heapSwap (int i, int j, Heap F) {
    int x = F->val[i];
    F->val[i] = F->val[j];
    F->ind[F->val[i]] = i;
    F->val[j] = x;
    F->ind[x] = j;
}


void heapBubbleDown(Heap F, int i) {
    int i_min;

    DEBUT:
    i_min = i;

    if (FG(i) >= F->n)
        return;

    if (F->lt(F->val[FG(i)], F->val[i_min]))
        i_min = FG(i);

    if ((FD(i) < F->n) && F->lt(F->val[FD(i)], F->val[i_min]))
        i_min = FD(i);

    if (i_min != i) {
        heapSwap(i, i_min, F);
        i = i_min;
        goto DEBUT;
    }
}


void makeHeap(Heap F) {
    int i;

    for (i = (F->n-1)/2; i >= 0; i --)
        heapBubbleDown(F, i);
}


int heapExtractMin (Heap F) {
    int min = F->val[0];

    F->ind[min] = NONE;

    F->n --;
    F->val[0] = F->val[F->n];
    F->ind[F->val[0]] = 0;

    heapBubbleDown(F, 0);
    //heapVerif(F, weight);

    return min;
}



void heapRemove (int x, Heap F) {
    int i = F->ind[x];

    F->ind[x] = NONE;

    F->n --;

    if (i == F->n)
        return;

    F->val[i] = F->val[F->n];
    F->ind[F->val[i]] = i;

    if (F->lt(x, F->val[i])) //(weight[F->val[i]] > weight[x])
        heapBubbleDown(F, i);
    else
        heapBubbleUp(i, F);
    //heapVerif(F, weight);
}


void heapJustRemove (int x, Heap F) {
    int i = F->ind[x];

    F->ind[x] = NONE;

    F->n --;

    if (i == F->n)
        return;

    F->val[i] = F->val[F->n];
    F->ind[F->val[i]] = i;
}

void heapRemoveAll(Heap heap) {
    for (int i = 0; i < heap->n; i ++)
        heap->ind[heap->val[i]] = NONE;
    heap->n = 0;
}


/* Decrease the value of x to w. */
void heapBubbleUp (int i, Heap F) {
    while ( (i > 0) && (F->lt(F->val[i], F->val[PERE(i)])) ) { // (weight[F->val[PERE(i)]] > w) ) {
        heapSwap(PERE(i), i, F);
        i = PERE(i);
    }
}


/* Decrease the value of x to w. */
void heapDecreaseKey (int x, Heap F) {
    heapBubbleUp(F->ind[x], F);
}



void heapInsert (int x, Heap F) {
    F->val[F->n] = x;
    F->ind[x] = F->n;
    F->n ++;
    heapBubbleUp(F->n-1, F);
}


void heapJustAdd (int x, Heap F) {
    F->val[F->n] = x;
    F->ind[x] = F->n;
    F->n ++;
}




void heapVerif(Heap F, int *values) {
    for (int i = 0; i < F->n; i ++) {
        if ((FG(i) < F->n) && (F->lt(F->val[FG(i)], F->val[i]))) { //(w[F->val[i]] > w[F->val[FG(i)]])) {
            printf("heap mal configured %d/%d :: \n", F->val[i], F->val[FG(i)]);
            heapPrint(F, values, FG(i)+1, 1585189);
            exit(0);
        }
        if ((FD(i) < F->n) && (F->lt(F->val[FD(i)], F->val[i]))) { //(w[F->val[i]] > w[F->val[FD(i)]])) {
            printf("heap mal configured %d/%d :: \n", F->val[i], F->val[FD(i)]);
            heapPrint(F, values, FD(i)+1, 1585189);
            exit(0);
        }
    }
}


void heapPrint(Heap F, int *weight, int n, int infini) {
    for (int i = 0; i < n; i ++)
        if (weight[F->val[i]] >= infini)
            printf("- ");
        else
            printf("%d ", weight[F->val[i]]);
    printf("\n");
}

void heapPrintMax(Heap F, int *weight) {
    int max = weight[F->val[0]];
    printf("heap max=%d  ", max);
    for (int i = 0; i < F->n; i ++)
        if (weight[F->val[i]] == max)
            printf("%d ", F->val[i]);
    printf("\n");
}



int isInHeap(int x, Heap F) {
    return (F->ind[x] != NONE);
}



//
// minheap :: Heap used to partition the elements that have the value min from the others (for a given evaluation val[])
//

// unused
int minheapCalculMin(Heap F, int val[]) {
    int min = val[F->val[0]];
    for (int i = 1; i < F->n; i ++)
        if (val[F->val[i]] < min) min = val[F->val[i]];
    return min;
}

// Recalculate min after all min have been removed, bound is the minimal value of the min
void minheapUpdateMin(Heap F, int bound, int val[]) {
    int newmin = val[F->val[0]];
    if (newmin > bound) {
        for (int i = 1; i < F->n; i++)
            if (val[F->val[i]] < newmin) {
                newmin = val[F->val[i]];
                if (newmin == bound) break;
            }
    }
    F->min = newmin;
}

void minheapSearchAndPackMins(Heap F, int val[]) {
    F->min = val[F->val[0]];
    for (int i = 1; i < F->n; i ++)
        if (val[F->val[i]] < F->min) F->min = val[F->val[i]];
    F->nbmin = 0;
    for (int i = 0; i < F->n; i ++)
        if (val[F->val[i]] == F->min) {
            heapSwap(F->nbmin, i, F);
            F->nbmin ++;
        }
}



void minheapPackMins(Heap F, int val[]) {
    // Suppose that F->min is up to date
    F->nbmin = 0;
    for (int i = 0; i < F->n; i ++)
        if (val[F->val[i]] == F->min) {
            heapSwap(F->nbmin, i, F);
            F->nbmin ++;
        }
}


void minheapRemove(int x, Heap F, int val[]) {
    int i = F->ind[x];
    int *p = F->val + i;

    F->ind[x] = NONE;

    F->n --;
    //if (F->n == 0) { F->nbmin = 0; return; }

    if (val[x] > F->min) {
        // x is not a min
        if (i < F->n) {
            *p = F->val[F->n];
            F->ind[*p] = i;
        }
        return;
    }

    // x is a min

    F->nbmin --;

    // x was the last of the list
    if (i == F->n) return;

    // x was the uniq element with min value
    if (F->nbmin == 0) {
        // copy last element at position i and rebuild mins
        int last = F->val[F->n];
        *p = last;
        F->ind[last] = i;
        minheapUpdateMin(F, F->min+1, val);
        minheapPackMins(F, val);
        return;
    }

    // Copy last min at the position i
    if (i != F->nbmin) {
        *p = F->val[F->nbmin];
        F->ind[*p] = i;
    }
    // Copy last non min element after the mins if necessary
    if (F->n > F->nbmin) {
        F->val[F->nbmin] = F->val[F->n];
        F->ind[F->val[F->nbmin]] = F->nbmin;
    }
}


void minheapJustRemove(int x, Heap F, int val[]) {
    int i = F->ind[x];
    int *p = F->val + i;

    F->ind[x] = NONE;

    F->n --;

    if (val[x] > F->min) {
        // x is not a min
        if (i < F->n) {
            *p = F->val[F->n];
            F->ind[*p] = i;
        }
        return;
    }

    // x is a min

    F->nbmin --;

    // x was the last of the list
    if (i == F->n) return;

    // Copy last min at the position i
    if (i != F->nbmin) {
        *p = F->val[F->nbmin];
        F->ind[*p] = i;
    }
    // Copy last non min element after the mins if necessary
    if (F->n > F->nbmin) {
        F->val[F->nbmin] = F->val[F->n];
        F->ind[F->val[F->nbmin]] = F->nbmin;
    }
}


// The value val[x] of x has decreased strictly
void minheapDecreaseValue(int x, Heap F, int val[]) {

    if (val[x] < F->min) {
        F->min = val[x];
        F->nbmin = 1;
        if (F->ind[x] > 0) heapSwap(0, F->ind[x], F);
        return;
    }
    if (val[x] == F->min) {
        if (F->ind[x] >= F->nbmin) heapSwap(F->nbmin, F->ind[x], F);
        F->nbmin ++;
    }
}















