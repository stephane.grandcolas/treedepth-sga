/*
 *  lire.c
 *  graphes
 *
 *  Created by Stéphane on 12/02/18.
 *  Copyright 2018 __MyCompanyName__. All rights reserved.
 *
 */

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "graph.h"
#include "lists.h"
#include "lire.h"
#include "main.h"


/*
 SECTION Graph
 Nodes 6405
 Edges 10454
 E 1 2 5
 ...
*/


int countGraphe(FILE *f)
{
    int nb_sommets, nb_aretes;

    fscanf(f, "%*s%*s%d%d", &nb_sommets, &nb_aretes);

    return nb_sommets;
}


Graph lireGraphe(FILE *f)
{
    int s1, s2;
    int nb_sommets, nb_aretes, nba;
    LIST *adj;

    //fscanf(f, "%",&s1, &s2)
    fscanf(f, "%*s%*s%d%d", &nb_sommets, &nb_aretes);

    int *nadj = calloc(nb_sommets, sizeof(int));

    adj = calloc(nb_sommets, sizeof(LIST));
    assert(adj != NULL);

    nba = nb_aretes;
    while (1) {
        if (fscanf(f, "%d%d",&s1, &s2) != 2)
            break;
        s1 --; s2 --;
        adj[s2] = newLNode( s1, adj[s2], 1);
        adj[s1] = newLNode( s2, adj[s1], 1);

        nadj[s1] ++;
        nadj[s2] ++;

        nba --;
    }

    //assert(nba == 0);

    Graph g = newGraph(nb_sommets, nb_aretes, adj, nadj);

    return g;
}

