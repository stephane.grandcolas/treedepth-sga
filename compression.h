//
// Created by Stephane on 14/03/2020.
//

#ifndef SRC_COMPRESSION_H
#define SRC_COMPRESSION_H


#include "graph.h"
#include "tree.h"


int pullUpChildren(Node node, Graph g, int depth);
void exploreAndCompress(Node node, Graph g, int depth);


#endif //SRC_COMPRESSION_H
