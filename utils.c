//
// Created by Stephane on 10/03/2020.
//

#include <assert.h>
#include <string.h>
#include <stdio.h>

#include "utils.h"
#include "sets.h"


// choose at random a vertex from S[], with preference for those that have a big weight
int chooseWeightedRandom(int weights[], int S[], int n, int total) {
    assert(total > 0);
    int t = rand()%total;
    int i = 0;
    while (t >= weights[S[i]]) {
        t -= weights[S[i]];
        i ++;
    }
    return S[i];
}



void copyList(int *dest, int *src, int n) {
    memcpy(dest, src, n * sizeof(int));
}


void printList(int S[], int n) {
    for (int i = 0; i < n; i ++) printf("%d ", S[i]);
    printf("\n");
}

void shakeList(int S[], int pos[], int n, int nSteps) {
    while (nSteps -- > 0) {
        int i = rand()%n;
        int j = rand()%n;
        swap(S, i, j);
        pos[S[i]] = i;
        pos[S[j]] = j;
    }
}


int nbOccs(int x, int S[], int n) {
    int nb = 0;
    for (int i = 0; i < n; i ++)
        if (S[i] == x)
            nb ++;
    return nb;
}


int indexMaxInList(int S[], int n) {
    int imax = 0;
    for (int i = 1; i < n; i ++)
        if (S[i] > S[imax])
            imax = i;
    return imax;
}


int maxInList(int S[], int n) {
    int max = S[0];
    for (int i = 1; i < n; i ++)
        if (S[i] > max)
            max = S[i];
    return max;
}


// make list with elements of S[],n with value num in I[]
int extractList(int num, int S[], int n, int C[], int T[]) {
    int size = 0;
    for (int i = 0; i < n; i ++) {
        if (C[i] == num)
            T[size ++] = S[i];
    }
    return size;
}

// the vertices in S[] with num num are placed at the beginning of S
int packListLeft(int num, int S[], int n, int C[]) {
    int nb = 0;
    for (int i = 0; i < n; i ++) {
        if (C[S[i]] == num) {
            swap(S, i, nb);
            nb ++;
        }
    }
    return nb;
}


// move a portion of the array at the left
void copyListLeft(int S[], int start, int size) {
    for (int i = 0; i < size; i ++)
        S[i] = S[i+start];
}



int nbInListWithThisValue(int val, int *S, int n, int *values) {
    int nb = 0;
    for (int i = 0; i < n; i ++) {
        if (values[S[i]] == val) nb ++;
    }
    return nb;
}



// search in set, either a SET of a sorted list
int isInSet(int v, SET S, int T[], int n) {

    if (S != NULL) {
        return IN(v, S);
    }

    if (n == 0) return 0;

    int l = 0;
    int r = n;
    // l <= pos < r
    while (l+1 < r) {
        int m = (l+r)/2;
        if (v >= T[m])
            l = m;
        else
            r = m;
    }
    // l+1=r and l <= pos < r
    return (v == T[l]);
}



// search v in the list,
int isInList(int v, int T[], int n) {
    int l = 0;
    int r = n;
    while (l+1 < r) {
        int m = (l+r)/2;
        if (v >= T[m])
            l = m;
        else
            r = m;
    }
    // l+1=r and l <= pos < r
    return (v == T[l]);
}





//
//
//


// Introsort: the sort function of C++ STL.


void swap(int T[], int i, int j) {
    int tmp = T[i];
    T[i] = T[j];
    T[j] = tmp;
}

void swapValues(int *p, int *q) {
    int tmp = *p;
    *p = *q;
    *q = tmp;
}


// heapify heap T[i],...,T[b-1]
void heapify(int i, int T[], int n) {
    while (2*i+1 < n) {
        int imin = i;
        if (T[2*i+1] < T[imin]) imin = 2*i+1;
        if ((2*i+2 < n) && (T[2*i+2] < T[imin])) imin = 2*i+2;
        if (imin == i) break;
        swap(T, i, imin);
        i = imin;
    }
}

// Make a heap with elements T[a],...,T[n-1]
void make_heap(int T[], int n) {
    for (int i = (n+1)/2; i >= 0; i --) {
        heapify(i, T, n);
    }
}

// Sort T[a],...,T[n-1]
void sort_heap(int T[], int n) {
    for (int i = n-1; i >= 1; i --) {
        swap(T, 0, i);
        heapify(0, T, i);
    }
    for (int i = 0; i < n/2; i ++) swap(T, i, n-1-i);
}

// Sort arr[a],...,arr[b]
void InsertionSort(int S[], int a, int b) {
    for (int i = a+1; i <= b; i++) {

        int key = S[i];
        int j = i-1;
        while (j >= a && S[j] > key) {
            S[j+1] = S[j];
            j = j-1;
        }
        S[j+1] = key;
    }
}


// A function to partition the array and return the partition point
int* Partition(int arr[], int low, int high)
{
    int pivot = arr[high];    // pivot
    int i = (low - 1);  // Index of smaller element

    for (int j = low; j <= high-1; j++) {
        // If current element is smaller than or equal to pivot

        if (arr[j] <= pivot) {
            // increment index of smaller element
            i++;

            swapValues(arr+i, arr+j);
        }
    }
    swapValues(arr+i+1, arr+high);
    return (arr + i + 1);
}


// A function that find the middle of the values pointed by the pointers a, b, c and return that pointer
int * MedianOfThree(int * a, int * b, int * c)
{
    if (*a < *b && *b < *c)
        return (b);

    if (*a < *c && *c <= *b)
        return (c);

    if (*b <= *a && *a < *c)
        return (a);

    if (*b < *c && *c <= *a)
        return (c);

    if (*c <= *a && *a < *b)
        return (a);

    //if (*c <= *b && *b <= *c)
        return (b);
}

// A Utility function to perform intro sort
void IntrosortUtil(int arr[], int * begin, int * end, int depthLimit)
{
    // Count the number of elements
    int size = end - begin + 1;

    // If partition size is low then do insertion sort
    if (size < 16) {
        InsertionSort(arr, begin-arr, end-arr);
        return;
    }

    // If the depth is zero use heapsort
    if (depthLimit == 0) {
        int n = (end-arr)+1;
        make_heap(arr, n);
        sort_heap(arr, n);
        return;
    }

    // Else use a median-of-three concept to
    // find a good pivot
    int * pivot = MedianOfThree(begin, begin+size/2, end);

    // Swap the values
    //swap(arr, pivot-arr, end-arr);
    swapValues(pivot, end);

    // Perform Quick Sort
    int * partitionPoint = Partition(arr, begin-arr, end-arr);
    IntrosortUtil(arr, begin, partitionPoint-1, depthLimit - 1);
    IntrosortUtil(arr, partitionPoint + 1, end, depthLimit - 1);

    return;
}


#include <math.h>

// Implementation of introsort: sort elements *begin,...,*end
void Introsort(int arr[], int *begin, int *end) {

    int depthLimit = (int) (2 * log(end-begin));

    // Perform a recursive Introsort
    IntrosortUtil(arr, begin, end, depthLimit);

    return;
}


void QSort1(int *T, int *V, int Deb, int Fin)
{
    int a, b;

    while (1)
    {
        a = Deb, b = Fin;
        int v = V[T[(Deb+Fin)/2]];
        do
        {
            while (V[T[a]] < v)
                a++;
            while (V[T[b]] > v)
                b--;
            if (a <= b) {
                int tmp = T[a];
                T[a] = T[b];
                T[b] = tmp;
                a++, b--;
            }
        }
        while (a <= b);

        if (b-Deb > Fin-a) {
            if (Fin > a)
                QSort1(T, V, a, Fin);
            if (Deb < b)
                Fin = b;
            else
                break;
        }
        else {
            if (Deb < b)
                QSort1(T, V, Deb, b);
            if (Fin > a)
                Deb = a;
            else
                break;
        }
    }
}






