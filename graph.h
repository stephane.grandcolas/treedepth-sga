
#ifndef _H_graph
#define _H_graph

/*
 *  graph.h
 *  graphes
 *
 *  Created by St�phane on 19/02/18.
 *  Copyright 2018 __MyCompanyName__. All rights reserved.
 *
 */


#include "lists.h"
#include "heap.h"
#include "sets.h"



#define ALL_TERMINALS 0
#define ONLY_LEAVES 1

// Choice of a vertex (choose at random among the best or use a weighted random choice)
#define MAX_CONNECTED_WEIGHTED_AT_RANDOM 0
#define MAX_CONNECTED_BEST_AT_RANDOM 1



typedef struct graph * Graph;

struct graph {
    int n; /* nb vertices */
    int m; /* nb edges */
    LIST *adj;
    int **slists; // neighbors sorted lists
    int **lists; // neighbors lists (reordered before separation to place first current vertices)
    int *nadj;

    SET *neighbors;
};


// For the heap sorted with numbers of connections
extern Heap conHeap;
extern int *nbN4Con; // for each vertex the number of its neighbors in the cluster
extern int nbE4Con, nbMax4Con, maxNbN4Con;
extern int nbUnconnectedVertices;


Graph newGraph(int n, int m, LIST *adj, int *nadj);
int areNeighbours(int u, int v, Graph g);
void printGraph(Graph g);
int nbNeighborsInHeap(int u, Heap F, Graph g);
int nbVerticeInHeap(int list[], int n, Heap F, Graph g);
int nbNeighborsInList(int u, SET V, int *S, int n, Graph g);
int hasNoNeighborInSet(int vertex, SET V, Graph g);
int hasNeighborsInSubtree(int vertex, SET V, Graph g);
int haveSameNeighbors(int u, int v, Graph g);

int hasMoreConnections(int a, int b);
void allocConnectionsHeap(Graph g);
int updateHeapNbNAfterRemoval(int u, Heap heap, Graph g);
int vertexWithManyConnections(Heap heap, int mode);
void updateNbMaxConInHeap(Heap H);
int rebuildConHeap(Heap heap);
void initConHeap(Graph g);
int vertexMaxConnectivity(int *S, int n, SET V, Graph g);
float connectivity(int u, int S[], int n, Graph g);

#endif





