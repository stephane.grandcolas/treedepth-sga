
DEBUG=no

ifeq ($(DEBUG),yes)
   CFLAGS= -I sga -g -Wall -pedantic -std=c99
else
   CFLAGS= -I sga -std=c99 -O3
endif


ifeq ($(PACE),yes)
   PACE_FLAG= -DPACE_2020
endif


all: treedepth 


treedepth: obj/main.o obj/lists.o obj/lire.o  obj/graph.o obj/fibheap.o obj/tree.o obj/separator.o obj/decompose.o obj/heap.o obj/sets.o obj/utils.o obj/compression.o obj/components.o obj/swaps.o
	gcc $(CFLAGS) obj/main.o obj/lists.o obj/lire.o obj/graph.o obj/fibheap.o obj/tree.o obj/separator.o obj/decompose.o obj/heap.o obj/sets.o obj/utils.o obj/compression.o obj/components.o obj/swaps.o -o treedepth

obj/swaps.o: sga/swaps.c sga/swaps.h sga/graph.h sga/utils.h sga/sets.h sga/lists.h sga/tree.h sga/decompose.h sga/main.h 
	gcc $(CFLAGS) -c sga/swaps.c -o obj/swaps.o
	

obj/components.o: sga/components.c sga/components.h sga/graph.h sga/utils.h sga/sets.h sga/lists.h sga/heap.h
	gcc $(CFLAGS) -c sga/components.c -o obj/components.o

obj/compression.o: sga/compression.c sga/compression.h sga/graph.h sga/utils.h sga/tree.h sga/sets.h
	gcc $(CFLAGS) -c sga/compression.c -o obj/compression.o

obj/utils.o: sga/utils.c sga/utils.h
	gcc $(CFLAGS) -c sga/utils.c -o obj/utils.o

obj/sets.o: sga/sets.c sga/sets.h 
	gcc $(CFLAGS) -c sga/sets.c -o obj/sets.o

obj/decompose.o: sga/decompose.c sga/decompose.h sga/separator.h sga/graph.h sga/utils.h sga/tree.h sga/compression.h sga/separator.h sga/heap.h sga/main.h sga/components.h sga/swaps.h 
	gcc $(CFLAGS) $(PACE_FLAG) -c sga/decompose.c -o obj/decompose.o

obj/separator.o: sga/separator.c sga/separator.h sga/graph.h sga/utils.h sga/components.h sga/main.h sga/heap.h
	gcc $(CFLAGS) -c sga/separator.c -o obj/separator.o

obj/tree.o: sga/tree.c sga/tree.h sga/graph.h sga/utils.h sga/sets.h sga/decompose.h 
	gcc $(CFLAGS) -c sga/tree.c -o obj/tree.o

obj/main.o: sga/main.c sga/lists.h sga/lire.h sga/graph.h sga/main.h 
	gcc $(CFLAGS) $(PACE_FLAG) -c sga/main.c -o obj/main.o

obj/graph.o: sga/graph.c sga/graph.h sga/lists.h
	gcc $(CFLAGS) -c sga/graph.c -o obj/graph.o

obj/lists.o: sga/lists.c sga/lists.h 
	gcc $(CFLAGS) -c sga/lists.c -o obj/lists.o

obj/lire.o: sga/lire.c sga/lire.h sga/lists.h sga/graph.h sga/main.h
	gcc $(CFLAGS) -c sga/lire.c -o obj/lire.o

obj/fibheap.o: sga/fibheap.c sga/fibheap.h
	gcc $(CFLAGS) -c sga/fibheap.c -o obj/fibheap.o

obj/heap.o: sga/heap.c sga/heap.h
	gcc $(CFLAGS) -c sga/heap.c -o obj/heap.o 

clean:
	rm -f treedepth \
	rm -f obj/*.o


