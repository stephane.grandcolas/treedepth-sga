//
// Created by Stephane on 18/03/2020.
//

#ifndef SGA_MAIN_H
#define SGA_MAIN_H

#include <signal.h>
#include <stdio.h>
#include <time.h>


extern volatile sig_atomic_t stopSearch;

extern int pace_2020;

extern int noSwapsInFirstRuns;
extern int timeWithNoSwaps;

extern int algo;
extern int sizeSwitchToGreedy;

extern int time_limit;
extern clock_t startTime;

extern FILE *fileSolution;
extern int printSolution;
extern int printResult;


extern int nbRunsSeparation;
extern int maxTimeSeparation;
extern int nbFlushes;

extern int perForceChoiceNotInC;
extern int perChooseInCAtRandom;

extern int ratioMax;
extern int ratioMin;

extern int modeEvalSeparator;
extern double coeffHeightNbEdges;
extern int modeEvalAtRandom;

extern int searchSwapsInCriticalBranch;
extern int pullUpIndependentSubtrees;
extern int maxMakeSwapsRestarts;

extern int preliminaryGreedyDecomposition;
extern int maxSizePrelimGreedy;
extern int maxDepthGreedy;

extern int depthThreshold;

extern int printStats;

extern int compressTree;



#endif //SGA_MAIN_H
