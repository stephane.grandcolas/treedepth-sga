//
// Created by Stephane on 24/03/2020.
//

#ifndef SGA_COMPONENTS_H
#define SGA_COMPONENTS_H

#include "graph.h"
#include "heap.h"
#include "sets.h"


extern int * iComp;
extern int *compSizes;
extern int *compNbEdges;
extern int * tableSortVComp;

extern int sizeCompMax;
//extern int *firstOfComp;


void allocSearchConnectedComponents(Graph g);
int searchConnectedComponents(SET V, int S[], int n, Graph g);
int searchConnectedComponentsGreedy(SET V, Heap heap, Graph g, int removedVertex);
void compExplore(int u, int num, SET V, int S[], int n, Graph g, int depth);
void compExploreBFS(int num, SET V, int S[], int n, Graph g, int verif);
int searchConnectedComponentsInHeap(Heap H, int nbNiS[], Graph g);
void sortListByComponent(int S[], int n, int nbComp, int sizes[]);

#endif //SGA_COMPONENTS_H
