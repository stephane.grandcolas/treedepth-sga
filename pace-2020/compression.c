//
// Created by Stephane on 14/03/2020.
//




#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "compression.h"
#include "graph.h"
#include "tree.h"
#include "sets.h"
#include "utils.h"



// Pull up subtrees that are independent with the node



// Pull up node children which are independent with the vertex of node
int pullUpChildren(Node node, Graph g, int depth) {
    Node p, next;
    int v = node->vertex;
    int nbPU = 0;
    int trace = 1;

    int prevFHeight = node->father->height;


    p = node->fbs;
    while (p != NULL) {
        next = p->next;
        if (independent(v, p, g)) {
            if (pullUp(p)) { nbPU ++; if (trace) printf("pull up %d:%d->%d\n", p->vertex, depth, depth-1); }
        }
        p = next;
    }

    // the father of node has been updated

    if (nbPU > 0) {
        if ((node->father != NULL) && (node->father->father != NULL))
            decHeightNodeUpdate(node->father->father, prevFHeight);
    }
    return nbPU;
}


// Explore and pull up nodes children when it is usefull (that is if it concerns critical nodes).
// at this level some children of node may be pulled up

void exploreAndCompress(Node node, Graph g, int depth) {
    Node p;
    int height = node->height;
    int prevFHeight = (node->father != NULL) ? node->father->height : 0;
    int trace = 0;
    int nbPU;

    if (trace) printf("[%d] DEB compress h=%d\n", depth, node->height);

    // For the height of node to decrease critical children of node must be either compressed
    // or pulled up.

    //START:
    p = node->fbs;
    nbPU = 0;
    while (p != NULL) {
        if (p->height+1 == height) { // In the other case p is not involved in the height of node
            exploreAndCompress(p, g, depth+1);
            if (((node->father != NULL)) && independent(node->vertex, p, g)) {
                if (pullUp(p)) { nbPU ++; if (trace) printf("pull up %d:%d->%d\n", p->vertex, depth, depth-1); }
            }
        }
        p = p->next;
    }

    if ( ! verifyTree(node, depth)) { printf("verify2 \n"); exit(0); }

    //if ( ! verifyTree(node->father, depth-1)) { printf("verify3 father\n"); exit(1); }

    if ((node->father != NULL) && (node->father->height != prevFHeight) && (node->father->father != NULL)) {
        decHeightNodeUpdate(node->father->father, prevFHeight);
        if ( ! verifyTree(node->father, depth-1)) { printf("verify4 father\n"); exit(1); }
    }

    //if (nbPU > 0) goto START;

}


void exploreAndCompressOLD(Node node, Graph g, int depth) {
    Node p = node->fbs;
    int height = node->height;
    int trace = 1;

    if (trace) printf("[%d] DEB compress h=%d\n", depth, node->height);
    if ( ! verifyTree(node, depth)) { printf("verify1 \n"); exit(0); }

    // For the height of node to decrease critical children of node must be either compressed
    // or pulled up.

    while (p != NULL) {
        if (p->height+1 == height) { // In the other case p is not involved in the height of node
            exploreAndCompress(p, g, depth+1);
        }
        p = p->next;
    }
    // Here, node has perhaps new children (that were children of some children) an is up to date

    if (node->father != NULL)
        pullUpChildren(node, g, depth);

    // Here perhaps node lose children. If its height has not changed so does the height of its father.

    if ((node->height < height) && (node->father != NULL)) {
        if (trace) printf("update father :: node:%d->%d father:%d x %d\n",
                height, node->height, node->father->height, node->father->nbhmax);
        updateDecHeightOnBranch(node->father, node->height);
        if (node->father->father != NULL)
            forceUpdateDecHeightOnBranch(node->father->father, node->father->height);
    }
    if ( ! verifyTree(node, depth)) { printf("verify2 \n"); exit(0); }
    if (trace) {
        if (height != node->height)
            printf("[%d] END compress h=%d  height node %d : %d->%d\n", depth, node->height, node->vertex, height,
                   node->height);
        else
            printf("[%d] END compress h=%d\n", depth, node->height);
    }

    if ( ! verifyTree(node->father, depth-1)) {
        printf("verify3 father\n");
        exit(1);
    }
}


















