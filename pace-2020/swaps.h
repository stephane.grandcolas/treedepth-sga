//
// Created by Stephane on 08/04/2020.
//

#ifndef SGA_CRITICAL_PATH_H
#define SGA_CRITICAL_PATH_H

#include "graph.h"
#include "tree.h"
#include "sets.h"




extern int nbCallsSwapNode;

Node makeSwapsInCriticalBranch(Node root, Graph g);
int listSwaps(Node root, int date, int maxSwaps, int maxSearches, Graph g);
Node makeSwaps(Node root, int nbSwaps, Graph g);
int selectSwaps(int *nodes, int *corr, int nb, int maxLen);
Node swapCriticalNode(Node root, Node node, Node cc, Graph g);
int evaluateSwap(Node node, Node cc, Graph g);
Node markCriticalBranch(Node root, int date, int single, Graph g);
int verifyCriticalBranch(Node root, Node cc, int date, Graph g);
Node searchCriticalCorrespondant(Node node, int *maxHeight, int date, int store, Graph g);

#endif //SGA_CRITICAL_PATH_H
