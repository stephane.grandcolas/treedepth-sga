//
// Created by Stephane on 08/04/2020.
//

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

#include "swaps.h"
#include "decompose.h"
#include "graph.h"
#include "main.h"
#include "tree.h"
#include "sets.h"
#include "utils.h"



// Parameters
int maxSwaps = 5; // max number of good swaps to find each time
int maxSwapsSearches = 11111; // max number of searches to perform each time
int justSwapBestSwap = 1; // the best swap is the one with the greatest gain

int nbCallsSwapNode = 0;


int depthCC;
Node bestNode; // best node and its correspondent for swapping
int *swapsVertices = NULL, *correspondent;
Node * dependentSubtrees;


Node * criticalBranch = NULL;
Node criticalNode;
int lengthCB;
int changeCriticalNode;
Node * lastAncestor; // the last critical ancestor that was calculated for the node
int * dateLastAncestor; // and at what date
int * neighborsAreSorted;
int * dateNodesSet; // last time the set of nodes in subtree has been updated
int * datePulledUp; // last time the subtree has been pulled up


// Two swaps may be incompatible, in paticular if their interval (node,correspondent) are not stricly disjoint.
// It can be detected when the number of subtrees has decreased since the evaluation of the swap.
// To have less conflicts it is better to perform first swaps of small lengths.
int *swapGain = NULL;
int *nbSubtrees = NULL; // Number of dependent subtrees on the critical branch for the nodes of the critical branch
int *swapLength = NULL; // the length of the branch between the node and its correspondent
int *swapDepth = NULL; // depth of the correspondent

Heap swapsHeap = NULL;

int lenMin;
int gainMax;

int *nbDeeperN = NULL; // Number of deeper neighbors in the tree, for each vertex of the graph
int limitNbDeepestNeighbors = 6; // Unused ignore swap if there are more neighbors under



int with_assertions = 0;

int nbCallsPrune = 0;

int date = 0;


void exploreAndUpdateSetOfNodes(Node p, SET V, int date, int dates[]) {
    if (dates[p->vertex] == date) {
        addSet(p->nodes, V);
        return;
    }
    ADDe(p->vertex, V);
    Node q = p->fbs;
    while (q != NULL) {
        exploreAndBuildSetOfNodes(q, V);
        q = q->next;
    }
}


int pullUpIndependentSubtreesInTheBranch(Node root, Node from, int date, Graph g) {
    Node p;
    int nbMoves = 0;

    int trace = 0;
    nbCallsPrune ++;

    if (from == root) return 0;

    if (trace) printf("from node : %d @ %d h=%d    root heigh=%d\n", criticalNode->vertex, criticalNode->depth, criticalNode->height, root->height);

    p = from;
    while (p->father != root)  {
        // search p critical subtrees that contain no neighbor of p, to pull them up
        int nbNUnder = g->nadj[p->vertex] - nbNeighborsAbove(p, g);
        int nbPU = 0;
        Node q = p->fbs;
        Node prev = NULL;

        if ((clock()-startTime)/CLOCKS_PER_SEC >= time_limit) stopSearch = 1;
        if (stopSearch) break;

        while (q != NULL) {

            if (stopSearch) break;

            Node next = q->next;
            if ((datePulledUp[q->vertex] == date) || (q->height+1 < p->height)) { prev = q; q = next; continue; } // Can jump some neighbors here (not counted in nbNUnder)

            if ((q->nodes == NULL) || (dateNodesSet[q->vertex] != date)) {
                if (q->nodes == NULL) q->nodes = allocSet(g->n);
                else clearSet(q->nodes);
                exploreAndUpdateSetOfNodes(q, q->nodes, date, dateNodesSet);
                dateNodesSet[q->vertex] = date;
            }

            int nb = hasNeighborsInSubtree(p->vertex, q->nodes, g);

            if ((nb == 0) || (nbNUnder == 0)) {
                // pull up subtree q independent with p
                nbPU ++;
                datePulledUp[q->vertex] = date;

                if (trace) printf("pull up %d  node %d @ %d h=%d\n", nbPU, q->vertex, q->depth, q->height);

                // 1. detach q
                if (prev == NULL) p->fbs = q->next;
                else prev->next = q->next;
                q->next = NULL;
                dateNodesSet[p->vertex] = NONE;
                //if (p->height == q->height+1) { p->nbhmax --; if (p->nbhmax == 0) initializeHeight(p); }


                // 2. search destination (do not consider to put q as the new root => loops)
                Node dest = p->father;
                int up = 1;
                while (dest != root) {
                    if ( ! hasNoNeighborInSet(dest->vertex, q->nodes, g)) break;
                    dateNodesSet[dest->vertex] = NONE;
                    dest = dest->father;
                    up ++;
                }
                if (trace && (dest != NULL)) printf("dest +%d  attach %d under %d @ %d h=%d\n", up, q->vertex, dest->vertex, dest->depth, dest->height);


                // 3. reattach to the tree
                if (dest == NULL) {
                    // q becomes the new root => loops, should consider heights (not updated) to put one under the other
                    root->next = q->fbs;
                    root->father = q;
                    q->fbs = root;
                    q->father = q->next = NULL;
                    root = q;
                }
                else {
                    q->next = dest->fbs;
                    dest->fbs = q;
                    q->father = dest;
                }
            }
            else
                prev = q;

            nbNUnder -= nb;
            q = next;
        }
        if (nbPU > 0) {
            initializeHeight(p);
            dateNodesSet[p->vertex] = NONE;
        }
        nbMoves += nbPU;
        p = p->father;
    }

    return nbMoves;
}


Node pullUpIndependentSubtreesInTree(Node root, Graph g) {
    date ++;

    while (1) {
        int prevHeight = root->height;
        // criticalNode = markCriticalBranch(root, date, 1, g);
        Node deepest = leftmostDeepestNode(root);
        int nbPullUp = pullUpIndependentSubtreesInTheBranch(root, deepest, date, g);
        if ((clock()-startTime)/CLOCKS_PER_SEC >= time_limit) stopSearch = 1;
        if (stopSearch) break;
        if (nbPullUp <= 0) break;
        updateDepthsAndHeights(root, 0, 0); // Useless ?
        if (0) printf("nbPU=%d    %d --> %d\n", nbPullUp, prevHeight, root->height);
    }
    return root;
}




int compareSwaps(int i, int j) {
    if (swapDepth[i] < swapDepth[j]) return 1;
    return 0;
    if (swapGain[i] == 0) return 0;
    if (swapGain[j] == 0) return 1;
    if (swapLength[i] < swapLength[j]) return 1;
    if (swapLength[i] > swapLength[j]) return 0;
    return (swapGain[i] > swapGain[j]);
}


void allocForSwaps (Graph g) {
    criticalBranch = malloc(g->n * sizeof(Node));
    swapGain = malloc(g->n * sizeof(int));
    nbSubtrees = malloc(g->n * sizeof(int));
    swapLength = malloc(g->n * sizeof(int));
    swapDepth = malloc(g->n * sizeof(int));
    nbDeeperN = malloc(g->n * sizeof(int));
    swapsVertices = malloc(g->n * sizeof(int));
    correspondent = malloc(g->n * sizeof(int));
    dependentSubtrees = malloc(g->n * sizeof(Node));
    lastAncestor = malloc(g->n * sizeof(Node));
    dateLastAncestor = calloc(g->n, sizeof(int));
    neighborsAreSorted = calloc(g->n, sizeof(int));
    dateNodesSet = calloc(g->n, sizeof(int));
    datePulledUp = calloc(g->n, sizeof(int));
    swapsHeap = allocHeap(g->n, compareSwaps);
}



int newVersionSearchingAboveFirstCriticalNode = 1;


Node makeSwapsInCriticalBranch(Node root, Graph g) {
    int nbPUpRestarts = 0;

    if (criticalBranch == NULL) allocForSwaps(g);

    int trace = 0;

    // updateNbDeeperNeighbors(nbDeeperN, g);
    //freeSetsOfNodes(theNodes, g->n);
    updateDepthsAndHeights(root, 0, 0);

    if (pullUpIndependentSubtrees) {
        PULL_UP:

        root = pullUpIndependentSubtreesInTree(root, g);

#ifdef NOTDEF
        date ++;

        while (1) {
            int prevHeight = root->height;
            // criticalNode = markCriticalBranch(root, date, 1, g);
            Node deepest = leftmostDeepestNode(root);
            int nbPullUp = pullUpIndependentSubtreesInTheBranch(root, deepest, date, g);
            if ((clock()-startTime)/CLOCKS_PER_SEC >= time_limit) stopSearch = 1;
            if (stopSearch) break;
            if (nbPullUp <= 0) break;
            updateDepthsAndHeights(root, 0, 0); // Useless ?
            if (trace) printf("nbPU=%d    %d --> %d\n", nbPullUp, prevHeight, root->height);
        }
#endif

        updateBestDecomposition(root, g);
    }

    if (stopSearch) return root;

    if ( ! searchSwapsInCriticalBranch)
        goto TERMINATE;

    int nbSwapsCompleted = 0;
    int height = root->height;
    while (1) {
        date ++;

        criticalNode = markCriticalBranch(root, date, 0, g); // sort neighbors on critical branch, built set of nodes for criticalNode

        if (stopSearch) break;

        //updateDepthsAndHeights(root, 0, 0);

        int nbSwaps = listSwaps(root, date, maxSwaps, maxSwapsSearches, g);

        if (nbSwaps == 0) break;

        if (stopSearch) break;

        if (nbSwaps > 0) {
            nbSwapsCompleted ++;
            if (justSwapBestSwap) {
                root = swapCriticalNode(root, bestNode, theNodes[correspondent[bestNode->vertex]], g);
            }
            else {
                nbSwaps = selectSwaps(swapsVertices, correspondent, nbSwaps, lenMin + 120 * root->height / 100);
                if (0)
                    root = swapCriticalNode(root, theNodes[swapsVertices[0]], theNodes[correspondent[swapsVertices[0]]], g);
                else
                    root = makeSwaps(root, nbSwaps, g);
            }
        }

        updateDepthsAndHeights(root, 0, 0);

        updateBestDecomposition(root, g);

        if (stopSearch) break;
        // if (root->height == height) break; // stop loop modified 2/05/2020
    }

    if (root->height < height) nbPUpRestarts = 0;

    if (nbSwapsCompleted > 0) { if (nbPUpRestarts ++ < maxMakeSwapsRestarts) goto PULL_UP; }

    if (trace) printf("terminate swaps  %.1fs\n", (float) (clock()-startTime)/CLOCKS_PER_SEC);

    //printf("nbPUpRestarts = %d\n", nbPUpRestarts);
    TERMINATE:
    //verifyDecomposition(root, g);
    //printf("swaps : %d --> %d\n", height0, root->height);
    freeSetsOfNodes(theNodes, g->n);

    return root;
}









// Select swaps so as there is no overlappings
int selectSwaps(int *nodes, int *corr, int nb, int maxLen) {
    resetHeap(swapsHeap);
    for (int i = 0; i < nb; i ++)
        heapJustAdd(swapsVertices[i], swapsHeap);
    makeHeap(swapsHeap);

    // Select swaps that are independent, that is the node of the second is deeper than the destination of the first one
    swapsVertices[0] = swapsHeap->val[0];
    int n = 1;
    int maxDepth = swapDepth[swapsVertices[0]];
    for (int i = 1; i < nb; i ++)
        //if (theNodes[swapsVertices[i]]->depth >= maxDepth)
        if (swapDepth[swapsHeap->val[i]] - swapLength[swapsHeap->val[i]] >= maxDepth) {
            //int v = swapsHeap->val[i];
            //if (theNodes[v]->depth != swapDepth[v] - swapLength[v]) printf("node depth=%d   swap depth=%d\n", theNodes[v]->depth, swapDepth[v] - swapLength[v]);
            swapsVertices[n] = swapsHeap->val[i];
            maxDepth = swapDepth[swapsVertices[n]];
            n ++;
        }
    return n;
}



#define SEARCH_FROM_ROOT

int listSwaps(Node root, int date, int maxSwaps, int maxSearches, Graph g) {

    gainMax = 0;
    lenMin = g->n+1;
    int nbSwaps = 0;
    int maxHeight;

#ifdef SEARCH_FROM_ROOT
    int depth = 0;
    for (Node node = root; node != criticalNode; node = node->fbs, depth++)
#else
    int depth = criticalNode->depth-1;
    for (Node node = criticalNode->father; node != NULL; node = node->father, depth--)
#endif
    {
        if (maxSearches -- == 0) break;

        Node cc = searchCriticalCorrespondant(node, &maxHeight, date, 0, g);

        int gain;

        if (cc != NULL) {
            gain = root->height - (maxHeight + 1 + cc->depth);

            if (gain <= 0) continue;

            if (gain > gainMax) {
                gainMax = gain;
                bestNode = node;
            }

            swapGain[node->vertex] = gain;
            swapLength[node->vertex] = cc->depth - node->depth;
            swapDepth[node->vertex] = cc->depth;
            if (swapLength[node->vertex] < lenMin) lenMin = swapLength[node->vertex];
            swapsVertices[nbSwaps] = node->vertex;
            correspondent[node->vertex] = cc->vertex;
            nbSwaps ++;
            if (nbSwaps == maxSwaps) break;
        }

        if ((clock()-startTime)/CLOCKS_PER_SEC >= time_limit) stopSearch = 1;
        if (stopSearch) return 0;
    }
    return nbSwaps;
}




int nbIgnoredSwaps;

Node makeSwaps(Node root, int nbSwaps, Graph g) {

    int moved[g->n]; // mark moved nodes, in case the node to move had get new children

    for (int i = 0; i < nbSwaps; i ++)
        moved[swapsVertices[i]] = 0;

    nbIgnoredSwaps = 0;
    for (int i = 0; i < nbSwaps; i ++) {
        if (moved[swapsVertices[i]]) continue;
        moved[correspondent[swapsVertices[i]]] = 1;

        root = swapCriticalNode(root, theNodes[swapsVertices[i]], theNodes[correspondent[swapsVertices[i]]], g);

        if ((clock()-startTime)/CLOCKS_PER_SEC >= time_limit) {
            stopSearch = 1;
            return root;
        }
        if (changeCriticalNode) break;
        break;
    }
    //printf("nbSwaps=%d ignored=%d\n", nbSwaps, nbIgnoredSwaps);
    return root;
}



// Depths and heights are ok for critical nodes, not for the other nodes


Node swapCriticalNode(Node root, Node node, Node cc, Graph g) {
    //int vertex = node->vertex;
    Node father = node->father;
    Node substitute = node->fbs;
    Node p;

    nbCallsSwapNode ++;

    int trace = 0;

    if (with_assertions) assert(cc != node);
    if (cc == node) return root;

    if (trace) printf("swap %d@%d h%d -- %d@%d h%d  (h=%d)\n",
            node->vertex, node->depth, node->height, cc->vertex, cc->depth, cc->height, root->height);

    // 1. list dependent subtrees of node and remove them
    int bidon;
    searchCriticalCorrespondant(node, &bidon, date, 1, g); // with store=1, to build dependentSubtrees[]
    // dependentSubtrees contains all the subtrees to move, snatch them from the tree (also the children of node)
    for (Node *p = dependentSubtrees; *p != NULL; p ++)
        justRemoveChild(*p);

    // 2. remove node and substitute to node its critical child
    if (node != root) {
        father->fbs = substitute;
        Node last = substitute;
        while (last->next != NULL) { //last->depth --;
        last->father = father; last = last->next; }
        last->father = father;
        //last->depth --;
        if (node->next != NULL) {
            last->next = node->next;
            last = last->next;
            while (last != NULL) { last->father = father; last = last->next; }
            node->next = NULL;
        }
    } else { // substitute becomes the root, its siblings are new children
        root = substitute;
        substitute->father = NULL;
        Node last = substitute->fbs;
        while (last->next != NULL) { //last->depth --;
        last = last->next; }
        //last->depth --;
        if (substitute->next != NULL) {
            last->next = substitute->next;
            last = last->next;
            while (last != NULL) { last->father = substitute; last = last->next; }
            substitute->next = NULL;
        }
    }

    // 3. Add subtrees as children of node. Update node height
    resetNode(node);
    for (Node *p = dependentSubtrees; *p != NULL; p ++)
        addChild(*p, node);

    // 4. Attach node to the tree under its critical correspondent
    if (with_assertions) assert(cc->fbs != NULL);
    node->next = cc->fbs->next;
    cc->fbs->next = node;
    node->father = cc;
    node->depth = cc->depth+1;




    // 4. update heights, depths and sets of nodes

    // heights :: normally only the height of p and the heights from father to root can change.
    // depths :: on the critical branch the depths loose one for all the nodes under father (correspondent comprised),
    // and the depth of node becomes depth(corresp)+1, and its subtrees can also be updated (what is the usefulness of depth ?)

    changeCriticalNode = (node->height + 1 >= cc->height);

    // update depth on portion of critical branch criticalNode-cc
    p = criticalNode;
    while (p != cc) {
        p->depth --;
        p = p->father;
    }

    // p=cc, cc update
    if (p->height < node->height+1) p->height = node->height+1;
    p->depth --;
    p = p->father;

    // update depth and heights for nodes between cc and father
    while (p != father) {
        p->depth --;
        if (p->height < p->fbs->height+1) initializeHeight(p);
        p = p->father;
    }

    // nodes from father to root have smaller heights
    while (p != NULL) {
        p->height --;
        if ( ! changeCriticalNode) {
            // verify that there is not another child with max height, in which case the critical node changes
            Node q = p->fbs->next;
            while (q != NULL) {
                if (q->height + 1 == p->height) changeCriticalNode = 1;
                q = q->next;
            }
        }
        p = p->father;
    }

    if (with_assertions) {
        if (father == NULL) assert(root->depth == 0);
        else assert(substitute->depth == father->depth+1);
    }

    if (trace) {
        //printTree(father);
        printf("AFTER swap %d@%d h%d -- %d@%d h%d   father %d@%d h%d\n", node->vertex, node->depth, node->height,
               cc->vertex, cc->depth, cc->height, father->vertex, father->depth, father->height);
    }
    return root;
}







// search the bottom node of the critical path (path from root such that each node has a uniq critical child)
// Mark the nodes of the critical branch with the given date
Node markCriticalBranch(Node root, int date, int single, Graph g) {
    Node p = root;
    Node the = NULL;
    int height = root->height;
    int depth = 0; // the number of levels is depth+1
    int nbCritical;

    lengthCB = 0;

    while (1) {
        Node q = p->fbs;
        Node pred, thePred;
        nbCritical = 0;

        p->depth = depth;
        p->date = date;
        if (with_assertions) assert(p->height == height);

        free (p->nodes);
        p->nodes = NULL;

        criticalBranch[lengthCB++] = p;

        if ( ! neighborsAreSorted[p->vertex]) {
            int *list = g->slists[p->vertex];
            Introsort(list, list, list + g->nadj[p->vertex] - 1);
            neighborsAreSorted[p->vertex] = 1;
        }

        the = NULL;
        while (q != NULL) {
            if (q->height == height - 1) {
                nbCritical++;
                the = q;
                thePred = pred;
            }
            pred = q;
            q = q->next;
        }

        if ((single && (nbCritical != 1)) || (the == NULL))
            break;

        if (the != p->fbs) {
            // the is positionned as first child
            thePred->next = the->next;
            the->next = p->fbs;
            p->fbs = the;
        }

        p = the;
        height--;
        depth++;
    }

    depthCC = depth;

    if (0) if ( !  newVersionSearchingAboveFirstCriticalNode) {
        if (p->nodes == NULL) p->nodes = allocSet(g->n);
        else clearSet(p->nodes);
        exploreAndBuildSetOfNodes(p, p->nodes);
    }

    if ( ! neighborsAreSorted[p->vertex]) {
        int *list = g->slists[p->vertex];
        Introsort(list, list, list + g->nadj[p->vertex] - 1);
        neighborsAreSorted[p->vertex] = 1;
    }

    return p; // p is the deepest node of the critical branch
}


int verifyCriticalBranch(Node root, Node cc, int date, Graph g) {
    Node p = root;
    Node the = NULL;
    int height = root->height;
    int depth = 0; // the number of levels is depth+1
    int nbCritical;

    while (1) {
        Node q;
        nbCritical = 0;

        if (p->height != height) printf("critical branch : height not good : %d  (%d)\n", p->height, height);
        if (p->depth != depth) printf("critical branch : depth not good : %d  (%d)\n", p->depth, depth);
        p->depth = depth;
        p->date = date;

        q = p->fbs;
        while (q != NULL) {
            if (q->height == height - 1) {
                nbCritical++;
                the = q;
            }
            q = q->next;
        }

        if (nbCritical != 1) {
            if (p != cc) printf("critical branch : %d@%d is not critical (%d@%d is)\n", cc->vertex, cc->depth, p->vertex, p->depth);
            break;
        }

        if (the != p->fbs) {
            printf("critical branch : critical node %d@%d is not first child\n", the->vertex, the->depth);
        }

        p = the;
        height--;
        depth++;
    }
    height = p->height;
    updateHeights(p);
    if (p->height != height) printf("critical branch : critical node %d@%d h%d had wrong height %d\n", p->vertex, p->depth, p->height, height);

    return 1; // p is the deepest node of the critical branch
}





// Calculate the lowest node of the critical path such that under this node there is no neighbors of node
Node searchCriticalCorrespondant(Node node, int *maxHeight, int date, int store, Graph g) {
// Node searchCriticalCorrespondant(Node node, int treeHeight, int gainMin, int *maxHeight, int date, int store, Graph g) {
   int vertex = node->vertex;
    int maxHeightDepSubtree = 0; // the maximal height of the subtrees that are dependent with vertex

    Node *ccc = dependentSubtrees;
    Node cc = NULL;
    for (int *p = g->lists[vertex]; *p != NONE; p ++) {
        Node ngb = theNodes[*p], the;

        if (stopSearch) return NULL;

        if ((ngb->date == date) && (ngb->depth < node->depth)) continue;

        if (ngb->date == date) {// on the critical branch, deeper than node
            if ((cc == NULL) || (ngb->depth > cc->depth))
                cc = ngb;
        }
        else {
            // discover a subtree containing a neighbor of node.
            if (dateLastAncestor[*p] != date) {
                lastAncestor[*p] = criticalAncestor(ngb, date); // highest non critical ancestor (his father is on the critical branch)
                dateLastAncestor[*p] = date;
            }
            the = lastAncestor[*p];
            if ((store) && (the->date != -date)) { // Must store the. -date helps not adding the same node twice
                *ccc ++ = the;
                the->date = -date;
            }
            if ((cc == NULL) || (the->father->depth > cc->depth))
                cc = the->father;

            if (the->height > maxHeightDepSubtree)
                maxHeightDepSubtree = the->height;
        }
        //         if (cc->depth + maxHeightDepSubtree + gainMin >= treeHeight) return NULL;
    }
    *ccc = NULL;
    *maxHeight = maxHeightDepSubtree;
    return cc;
}











